from datetime import datetime, timedelta
from instanceobject import VelocityInfo
from toml import time_limit, counter_limit

def velocity_test(user_input, input_hash):
    
    current_time = datetime.now()
    
    if user_input in input_hash:
        counter = input_hash[user_input].get_counter()
        initial_time = input_hash[user_input].get_time()
        difference = current_time - initial_time
        
        if counter == counter_limit and timedelta.total_seconds(difference) < time_limit:
            return True
        elif difference.total_seconds() > time_limit:
            input_hash[user_input].__init__()
        else:
            input_hash[user_input].add_counter()
    else:
        input_hash[user_input] = VelocityInfo()
    return False