from datetime import datetime, timedelta

class VelocityInfo():
    
    def __init__(self, initial_datetime = datetime.now(), counter = 1):
        self.initial_datetime = initial_datetime
        self.counter = counter
        
    def get_counter(self):
        return self.counter
        
    def get_time(self):
        return self.initial_datetime

    def add_counter(self):
        self.counter += 1

    def __str__(self):
        return "Initial time: %s, Count: %s" % (self.initial_datetime, self.counter)