from velocitytest import velocity_test

input_hash = {}

def get_user_choice():
    # Let users know what they can do.
    print("\n*** Hello World - Velocity Test ***")
    print("\n[1] See hash map.")
    print("[2] Input a string and get a result back.")
    print("[q] Quit.")
    
    return input("\nWhat would you like to do? ")

def show_names():
    # Shows the names of everyone who is already in the list.
    print("\nHere are the strings I have stored.\n")
    for key in input_hash:
        print("The name is " + key + " and its " + input_hash[key].__str__())

def enter_string():
    user_input = input("\nEnter a string: ")
    result = velocity_test(user_input, input_hash)
    print(result)

### MAIN PROGRAM ###

choice = ''
while choice != 'q':    
    
    filename = input("Input the Config Filename: ")
    choice = get_user_choice()
    
    # Respond to the user's choice.
    if choice == '1':
        show_names()
    elif choice == '2':
        enter_string()
    elif choice == 'q':
        print("\nQuitting.")
    else:
        print("\nI didn't understand that choice.\n")