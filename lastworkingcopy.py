from datetime import datetime
from datetime import timedelta
 
class VelocityInfo:

	def __init__(self, initial_datetime, counter):
		self.initial_datetime = initial_datetime
		self.counter = counter
	
	def get_counter(self):
		return self.counter
	
	def get_initial_datetime(self):
	    return self.initial_datetime
		
	def __str__(self):
		return "Initial time: %s, Count: %s" % (self.initial_datetime, self.counter)

input_hash = {}

def reset_time(user_input):
    new_instance = VelocityInfo(datetime.now(), 1)
    input_hash[user_input] = new_instance

def velocity_test(user_input):
    
    current_time = datetime.now()
    
    if user_input in input_hash:
        
        counter = input_hash[user_input].get_counter()
        initial_time = input_hash[user_input].get_initial_datetime()
        difference = current_time - initial_time
        
        if counter == 9 and timedelta.total_seconds(difference) < 300:
            return True
        elif difference.total_seconds() > 300:
            reset_time(user_input)
        else:
            input_hash[user_input].counter += 1
    else:
        reset_time(user_input)
        
    return False

'''
******************
used for testing purposes only
******************

testData = ['James', 'Tony', 'Xavier', 'Erika', 'Diamond', 'James', 'Tony', 'Xavier', 'Erika', 'Diamond', 'James', 'Tony', 'Xavier', 'Erika', 'Diamond', 'James', 'Tony', 'Xavier', 'Erika', 'Diamond', 'James', 'Tony', 'Xavier', 'Erika', 'Diamond']

for string in testData:
	velocity_test(string)

for key in input_hash:
  print("the name is " + key + " and its " + input_hash[key].__str__())
'''