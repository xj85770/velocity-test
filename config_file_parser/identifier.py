def extension_identifier(filename):
    f_extns = filename.split(".")
    return f_extns[-1]

# test:
# print(extension_identifier('test.ini'))