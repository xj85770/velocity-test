from identifier import extension_identifier
from filetypes.iniparser import iniparser
from filetypes.jsonparser import jsonparser
# from filetypes.pythonparser import pythonparser
from filetypes.tomlparser import tomlparser
from filetypes.yamlparser import yamlparser

class ConfigFileObject():

    def __init__(self, filename):
        self.filename = filename
        self.extension = extension_identifier(filename)

    def get_name(self):
        return self.filename

    def get_ext(self):
        return self.extension

    def ini(self):
        iniparser(self.filename)

    def json(self):
        jsonparser(self.filename)

#    def python(self):
#        pythonparser(self.filename)

    def toml(self):
        tomlparser(self.filename)

    def yaml(self):
        yamlparser(self.filename)