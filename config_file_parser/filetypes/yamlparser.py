import ruamel.yaml as yaml

def yamlparser(filename):    
    config = yaml.safe_load(filename)
    time_limit = config['default']['seconds']
    counter_limit = config['default']['counter_limit']

    print(filename)