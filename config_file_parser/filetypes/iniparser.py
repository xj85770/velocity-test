try:
    import configparser
except:
    from six.moves import configparser
        
def iniparser(filename):
    config = configparser.ConfigParser()
    config.read(filename)
    time_limit = config.getint('default','seconds')
    counter_limit = config.getint('default','counter_limit')

    for section_name in config.sections():
        print('Config File Section(s):', section_name)
        print('  Option(s):', config.options(section_name))
        for name, value in config.items(section_name):
            print('  %s = %s' % (name, value))