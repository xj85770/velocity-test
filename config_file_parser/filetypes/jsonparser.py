import json

def jsonparser(filename):
    with open(filename, 'r'):
        config = json.load(filename)

    time_limit = config['seconds']
    counter_limit = config['counter_limit']

    print(config)