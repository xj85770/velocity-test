import toml

def tomlparser(filename):
    config = toml.load(filename)
    time_limit = config.get('seconds')
    counter_limit = config.get('counter_limit')

    for section in config:
        print(section + " = " + str(config[section]))