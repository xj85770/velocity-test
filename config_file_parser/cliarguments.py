import argparse

def argparser():
    # create a new argument parser
    parser = argparse.ArgumentParser(description="Simple argument parser")
    # add a new command line option, call it '-c' and set its destination to 'config'
    parser.add_argument("-c", action="store", dest="config_file", help="sets the configuration file destination")

    # get the result
    result = parser.parse_args()

    # since we set 'config_file' as destination for the argument -c, 
    # we can fetch its value like this (and print it, for example):
    print("The configuration file you're using is " + repr(result.config_file))

    # we will return it to use in our application:
    return result.config_file