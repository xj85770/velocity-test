# Velocity Test
- Input: String
- Output: Boolean

# Requirements
- [python 3](https://www.anaconda.com/distribution/)
- [toml python library](https://pypi.org/project/toml/) ```pip install toml```
- [yaml python library](https://yaml.readthedocs.io/en/latest/) ```pip install ruamel.yaml```

# How To Run
1. Change configuration file name in ```velocitytest.py``` to appropriate module name listed below
- EXAMPLE ```from configjson import time_limit, counter_limit ``` change only ```configjson```
2. Run ```python cliapplication.py``` in terminal

## Configuration files
1. INI - ```configini.py``` // ```test.ini```
2. TOML - ```configtoml.py``` // ```test.toml```
3. YAML - ```configyaml.py``` // none
4. JSON - ```configjson.py``` // ```test.json```

## Shell Session Output
```shell session
>>> python cliapplication.py
*** Hello World - Velocity Test ***

[1] See hash map.
[2] Input a string and get a result back.
[q] Quit.

What would you like to do?
```

# Other notes
lastworkingcopy.py is used for testing purposes only
